import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { FullLayoutComponent } from "./layouts/full/full-layout.component";
import { ContentLayoutComponent } from "./layouts/content/content-layout.component";

import { Full_ROUTES } from "./shared/routes/full-layout.routes";
import { CONTENT_ROUTES } from "./shared/routes/content-layout.routes";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '', component: FullLayoutComponent, data: { title: 'full Views' },
    children:
      [
        //   // Full_ROUTES.map(result => { return { ...result } }),
        // {
        //   path: 'example',
        //   loadChildren: () => import('./views/example1/example1.module').then(m => m.Example1Module)
        // },{
        //   path: 'getDoc',
        //   loadChildren: () => import('./views/get-document/get-document.module').then(m => m.GetDocumentModule)
        // },{
        //   path: 'dark-and-drop',
        //   loadChildren: () => import('./views/dark-and-drop/dark-and-drop.module').then(m => m.DarkAndDropModule)
        // },
      ],
    // children: Full_ROUTES
  },

  // { path: '', component: FullLayoutComponent, data: { title: 'full Views' }, children: Full_ROUTES },
  // { path: '', component: ContentLayoutComponent, data: { title: 'content Views' }, children: CONTENT_ROUTES },
  // { path: '**', redirectTo: 'dashboard/ecommerce-v1' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
