﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Edocweb.Models
{
    public class ApproveStatus
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]


        public long Id { get; set; }
        public string Name { get; set; }
    }
}
