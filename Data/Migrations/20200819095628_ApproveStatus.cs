﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Edocweb.Data.Migrations
{
    public partial class ApproveStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 205, DateTimeKind.Local).AddTicks(5856));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5787));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5842));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5848));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5850));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5851));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5852));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5854));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5856));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5857));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5858));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5860));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5861));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5862));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5864));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5866));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5868));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5869));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5870));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5872));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5873));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5874));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5875));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5880));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5881));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5883));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5884));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5885));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5886));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5888));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5890));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5892));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5893));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5895));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5896));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5897));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5898));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5899));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5901));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5908));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5909));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5910));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5912));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5913));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5915));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5916));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5917));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5924));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5925));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5927));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5928));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5930));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5932));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5933));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5934));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5936));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5937));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5939));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5940));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5941));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5942));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5944));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5945));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5951));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5953));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5954));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5955));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5956));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5958));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5959));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5960));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5964));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5965));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5966));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5967));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5969));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5970));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5971));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5972));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5974));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5976));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5977));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5979));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5980));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5981));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5983));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5984));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5987));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5988));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5989));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5990));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5992));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5993));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5994));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5995));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5997));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5998));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(5999));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6000));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6002));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6004));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6005));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6006));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6007));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6051));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6053));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6054));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6056));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6057));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6058));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6059));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6061));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6062));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6063));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6065));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6067));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6068));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6069));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6070));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6072));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6073));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6074));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6075));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6077));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6078));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6079));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 127L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6080));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 128L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6082));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 129L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6083));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 130L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6084));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 131L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6085));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 132L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6087));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 133L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6088));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 134L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6089));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 135L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6091));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 136L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6092));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 137L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6093));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 138L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6094));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 139L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6096));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 140L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6097));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 141L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6099));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 142L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6100));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 143L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6101));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 144L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6103));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 145L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6104));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 146L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6106));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 147L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6108));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 148L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6109));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 149L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6110));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 150L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6111));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 151L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6113));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 152L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6115));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 153L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6116));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 154L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6118));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 155L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6119));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 156L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6120));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 157L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6122));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 158L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6123));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 159L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6124));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 160L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6126));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 161L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6127));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 162L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6128));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 163L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6130));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 164L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6131));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 165L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6132));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 166L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6133));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 167L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6134));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 168L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6136));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 169L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6137));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 170L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6139));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 171L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6140));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 172L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6142));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 173L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6143));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 174L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6144));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 175L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6145));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 176L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6147));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 177L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6148));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 178L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6150));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 179L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6151));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 180L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6152));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 181L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6153));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 182L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6155));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 183L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6156));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 184L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6159));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 185L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6160));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 186L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6161));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 187L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6163));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 188L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6164));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 189L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6165));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 190L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6166));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 191L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6168));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 192L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6170));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 193L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6171));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 194L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6172));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 195L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6173));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 196L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6175));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 197L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6176));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 198L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6177));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 199L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6178));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 200L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6181));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 201L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6182));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 202L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6183));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 203L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6185));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 204L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6186));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 205L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6188));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 206L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6187));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 207L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6189));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 208L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6191));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 209L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6193));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 210L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6194));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 211L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6195));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 212L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6196));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 213L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6198));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 214L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6199));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 215L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6200));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 216L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6202));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 217L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6203));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 218L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6204));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 219L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6206));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 220L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6207));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 221L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6208));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 222L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6209));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 223L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6211));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 224L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6212));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 225L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6214));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 226L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6215));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 227L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6216));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 228L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6217));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 229L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6219));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 230L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6220));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 231L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6221));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 232L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6249));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 233L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6250));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 234L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6252));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 235L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6253));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 236L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6254));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 237L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6255));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 238L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6257));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 239L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6258));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 240L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6259));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 241L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6261));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 242L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6262));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 243L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6263));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 244L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6264));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 245L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6266));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 246L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6267));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 247L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6268));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 248L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6269));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 249L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6271));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 250L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6272));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 251L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6273));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 252L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6274));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 253L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6276));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 254L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6277));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 255L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6278));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 256L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6280));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 257L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6282));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 258L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6283));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 259L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6284));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 260L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6286));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 261L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6287));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 262L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6288));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 263L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6289));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 264L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6291));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 265L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6292));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 266L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6293));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 267L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6294));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 268L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6296));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 269L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6297));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 270L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6298));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 271L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6300));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 272L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6301));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 273L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6302));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 274L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6303));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 275L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6305));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 276L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6306));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 277L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6307));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 278L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6308));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 279L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6310));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 280L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6311));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 281L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6312));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 282L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6313));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 283L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6315));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 284L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6316));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 285L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6317));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 286L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6318));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 287L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6320));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 288L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6321));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 289L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6322));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 290L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6323));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 291L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6325));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 292L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6326));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 293L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6327));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 294L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6328));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 295L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6330));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 296L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6331));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 297L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6332));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 298L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6334));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 299L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6335));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 300L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6336));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 301L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6337));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 302L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6339));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 303L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6340));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 304L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6341));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 305L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6343));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 306L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6344));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 307L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6345));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 308L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6346));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 309L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6348));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 310L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6349));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 311L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6350));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 312L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6352));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 313L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6353));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 314L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6355));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 315L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6356));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 316L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6357));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 317L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6358));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 318L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6360));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 319L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6361));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 320L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6363));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 321L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6364));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 322L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6365));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 323L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6367));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 324L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6368));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 325L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6369));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 326L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6370));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 327L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6372));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 328L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6374));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 329L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6375));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 330L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6376));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 331L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6378));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 332L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6379));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 333L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6380));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 334L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6381));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 335L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6383));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 336L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6385));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 337L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6386));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 338L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6387));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 339L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6388));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 340L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6390));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 341L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6494));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 342L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6496));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 343L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6497));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 344L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6499));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 345L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6500));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 346L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6502));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 347L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6503));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 348L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6504));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 349L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6505));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 350L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6507));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 351L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6508));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 352L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6511));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 353L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6512));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 354L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6513));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 355L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6514));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 356L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6516));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 357L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6517));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 358L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6518));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 359L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6520));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 360L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6548));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 361L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6550));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 362L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6551));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 363L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6553));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 364L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6554));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 365L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6555));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 366L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6556));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 367L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6558));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 368L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6560));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 369L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6561));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 370L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6563));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 371L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6564));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 372L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6565));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 373L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6567));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 374L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6568));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 375L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6569));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 376L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6571));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 377L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6573));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 378L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6574));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 379L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6575));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 380L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6576));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 381L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6578));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 382L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6579));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 383L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6581));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 384L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6582));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 385L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6584));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 386L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6585));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 387L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6586));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 388L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6588));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 389L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6589));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 390L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6590));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 391L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6591));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 392L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6593));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 393L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6594));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 394L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6595));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 395L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6597));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 396L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6598));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 397L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6599));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 398L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6601));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 399L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6602));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 400L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6603));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 401L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6605));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 402L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6606));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 403L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6607));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 404L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6608));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 405L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6610));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 406L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6611));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 407L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6612));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 408L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6614));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 409L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6615));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 410L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6616));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 411L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6617));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 412L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6619));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 413L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6620));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 414L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6621));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 415L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6623));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 416L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6624));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 417L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6626));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 418L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6627));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 419L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6628));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 420L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6630));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 421L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6631));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 422L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6632));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 423L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6633));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 424L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6635));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 425L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6636));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 426L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6637));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 427L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6639));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 428L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6640));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 429L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6641));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 430L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6642));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 431L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6644));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 432L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6645));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 433L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6646));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 434L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6648));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 435L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6649));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 436L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6650));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 437L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6652));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 438L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6653));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 439L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6654));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 440L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6655));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 441L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6657));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 442L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6658));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 443L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6659));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 444L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6661));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 445L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6662));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 446L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6663));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 447L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6664));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 448L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6666));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 449L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6667));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 450L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6668));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 451L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6669));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 452L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6671));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 453L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6672));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 454L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6673));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 455L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6675));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 456L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6676));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 457L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6677));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 458L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6679));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 459L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6680));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 460L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6681));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 461L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6682));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 462L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6684));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 463L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6685));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 464L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6686));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 465L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6688));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 466L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6689));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 467L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6690));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 468L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6691));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 469L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6693));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 470L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6694));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 471L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6695));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 472L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6697));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 473L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6698));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 474L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6699));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 475L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6700));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 476L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6702));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 477L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6703));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 478L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6704));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 479L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6706));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 480L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6707));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 481L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6708));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 482L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6710));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 483L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6712));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 484L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6713));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 485L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6714));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 486L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6715));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 487L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6717));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 488L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6744));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 489L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6745));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 490L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6747));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 491L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6748));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 492L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6749));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 493L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6751));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 494L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6752));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 495L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6753));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 496L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6755));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 497L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6756));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 498L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6757));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 499L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6758));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 500L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6760));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 501L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6761));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 502L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6762));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 503L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6764));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 504L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6765));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 505L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6766));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 506L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6768));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 507L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6769));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 508L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6860));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 509L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6861));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 510L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6863));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 511L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6864));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 512L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6865));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 513L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6870));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 514L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6871));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 515L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6872));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 516L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6873));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 517L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6875));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 518L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6876));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 519L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6877));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 520L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6878));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 521L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6880));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 522L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6881));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 523L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6882));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 524L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6884));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 525L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6885));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 526L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6886));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 527L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6887));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 528L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6889));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 529L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6890));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 530L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6891));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 531L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6892));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 532L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6894));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 533L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6895));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 534L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6896));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 535L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6898));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 536L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6899));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 537L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6900));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 538L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6902));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 539L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6903));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 540L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6904));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 541L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6906));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 542L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6907));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 543L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6908));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 544L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6909));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 545L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6911));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 546L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6912));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 547L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6913));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 548L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6914));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 549L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6916));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 550L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6917));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 551L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6918));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 552L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6919));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 553L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6921));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 554L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6922));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 555L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6923));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 556L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6924));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 557L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6926));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 558L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6927));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 559L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6928));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 560L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6929));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 561L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6931));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 562L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6932));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 563L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6933));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 564L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6934));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 565L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6936));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 566L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6937));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 567L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6938));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 568L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6940));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 569L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6941));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 570L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6942));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 571L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6943));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 572L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6945));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 573L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6946));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 574L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6947));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 575L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6948));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 576L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6950));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 577L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6951));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 578L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6952));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 579L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6953));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 580L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6954));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 581L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6956));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 582L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6957));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 583L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6958));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 584L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6959));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 585L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6961));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 586L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6962));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 587L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6963));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 588L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6965));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 589L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6966));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 590L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6967));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 591L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6968));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 592L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6970));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 593L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6971));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 594L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6972));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 595L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 206, DateTimeKind.Local).AddTicks(6973));

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 16, 56, 27, 207, DateTimeKind.Local).AddTicks(1113));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 378, DateTimeKind.Local).AddTicks(7760));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 2L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8616));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 3L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8675));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 4L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8678));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 5L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8679));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 6L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8681));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 7L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8682));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 8L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8684));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 9L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8685));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 10L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8687));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 11L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8688));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 12L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8690));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 13L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8691));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 14L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8693));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 15L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8694));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 16L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8695));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 17L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8697));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 18L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8698));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 19L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8699));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 20L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8700));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 21L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8701));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 22L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8703));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 23L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8704));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 24L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8706));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 25L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8707));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 26L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8708));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 27L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8709));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 28L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8711));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 29L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8712));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 30L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8713));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 31L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8714));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 32L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8716));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 33L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8717));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 34L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8719));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 35L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8720));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 36L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8721));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 37L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8724));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 38L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8725));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 39L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8727));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 40L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8728));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 41L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8729));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 42L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8731));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 43L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8732));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 44L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8733));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 45L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8734));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 46L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8778));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 47L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8779));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 48L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8781));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 49L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8782));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 50L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8783));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 51L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8784));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 52L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8786));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 53L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8787));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 54L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8788));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 55L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8789));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 56L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8791));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 57L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8792));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 58L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8793));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 59L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8795));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 60L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8796));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 61L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8797));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 62L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8798));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 63L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8799));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 64L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8801));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 65L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8803));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 66L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8804));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 67L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8806));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 68L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8807));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 69L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8808));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 70L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8809));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 71L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8810));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 72L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8812));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 73L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8813));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 74L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8815));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 75L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8816));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 76L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8817));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 77L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8818));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 78L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8819));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 79L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8821));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 80L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8822));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 81L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8824));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 82L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8825));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 83L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8826));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 84L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8827));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 85L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8829));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 86L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8830));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 87L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8831));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 88L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8833));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 89L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8834));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 90L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8835));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 91L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8836));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 92L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8837));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 93L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8839));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 94L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8840));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 95L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8841));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 96L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8843));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 97L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8844));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 98L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8845));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 99L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8846));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 100L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8848));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 101L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8849));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 102L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8850));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 103L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8852));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 104L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8853));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 105L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8854));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 106L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8856));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 107L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8857));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 108L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8858));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 109L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8859));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 110L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8861));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 111L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8862));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 112L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8864));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 113L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8865));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 114L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8866));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 115L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8867));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 116L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8869));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 117L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8870));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 118L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8871));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 119L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8872));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 120L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8874));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 121L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8875));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 122L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8877));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 123L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8878));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 124L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8879));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 125L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8881));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 126L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8882));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 127L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8883));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 128L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8885));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 129L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8886));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 130L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8887));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 131L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8889));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 132L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8890));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 133L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8891));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 134L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8892));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 135L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8893));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 136L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8895));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 137L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8896));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 138L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8898));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 139L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8899));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 140L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8900));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 141L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8902));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 142L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8903));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 143L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8904));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 144L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8905));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 145L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8907));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 146L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8908));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 147L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8909));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 148L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8910));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 149L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8912));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 150L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8913));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 151L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8914));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 152L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8916));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 153L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8917));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 154L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8918));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 155L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8919));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 156L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8920));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 157L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8922));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 158L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8923));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 159L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8924));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 160L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8925));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 161L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8929));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 162L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8930));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 163L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8932));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 164L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8933));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 165L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8934));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 166L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8936));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 167L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8937));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 168L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8938));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 169L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8939));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 170L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8941));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 171L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8942));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 172L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8943));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 173L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8944));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 174L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8971));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 175L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8972));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 176L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8974));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 177L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8975));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 178L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8977));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 179L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8978));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 180L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8980));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 181L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8981));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 182L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8982));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 183L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8983));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 184L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8984));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 185L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8986));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 186L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8987));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 187L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8988));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 188L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8989));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 189L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8991));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 190L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8992));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 191L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8993));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 192L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8995));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 193L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8996));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 194L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8997));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 195L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(8998));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 196L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9000));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 197L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9001));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 198L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9002));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 199L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9003));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 200L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9004));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 201L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9006));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 202L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9007));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 203L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9008));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 204L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9009));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 205L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9012));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 206L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9011));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 207L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9013));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 208L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9015));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 209L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9016));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 210L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9017));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 211L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9018));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 212L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9019));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 213L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9021));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 214L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9022));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 215L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9023));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 216L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9024));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 217L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9026));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 218L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9027));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 219L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9028));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 220L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9029));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 221L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9030));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 222L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9032));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 223L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9033));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 224L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9034));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 225L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9038));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 226L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9040));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 227L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9041));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 228L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9042));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 229L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9043));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 230L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9045));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 231L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9046));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 232L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9047));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 233L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9048));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 234L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9050));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 235L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9051));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 236L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9053));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 237L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9054));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 238L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9056));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 239L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9057));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 240L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9059));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 241L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9060));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 242L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9061));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 243L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9062));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 244L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9063));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 245L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9065));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 246L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9066));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 247L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9067));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 248L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9068));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 249L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9069));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 250L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9071));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 251L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9072));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 252L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9073));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 253L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9074));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 254L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9076));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 255L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9077));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 256L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9078));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 257L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9079));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 258L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9080));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 259L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9082));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 260L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9083));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 261L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9084));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 262L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9085));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 263L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9087));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 264L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9088));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 265L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9089));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 266L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9090));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 267L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9091));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 268L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9093));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 269L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9094));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 270L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9095));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 271L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9096));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 272L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9098));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 273L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9099));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 274L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9100));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 275L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9101));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 276L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9102));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 277L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9104));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 278L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9105));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 279L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9106));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 280L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9108));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 281L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9109));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 282L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9110));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 283L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9111));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 284L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9113));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 285L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9114));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 286L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9115));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 287L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9116));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 288L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9117));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 289L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9119));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 290L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9120));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 291L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9121));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 292L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9122));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 293L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9123));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 294L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9125));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 295L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9126));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 296L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9127));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 297L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9128));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 298L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9130));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 299L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9131));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 300L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9132));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 301L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9158));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 302L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9160));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 303L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9162));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 304L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9163));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 305L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9165));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 306L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9166));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 307L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9167));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 308L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9168));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 309L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9170));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 310L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9171));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 311L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9172));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 312L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9174));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 313L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9176));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 314L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9177));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 315L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9178));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 316L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9180));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 317L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9182));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 318L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9183));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 319L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9187));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 320L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9188));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 321L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9190));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 322L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9191));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 323L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9192));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 324L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9193));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 325L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9194));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 326L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9196));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 327L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9197));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 328L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9199));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 329L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9200));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 330L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9201));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 331L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9202));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 332L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9204));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 333L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9205));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 334L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9206));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 335L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9207));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 336L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9209));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 337L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9210));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 338L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9212));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 339L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9213));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 340L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9214));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 341L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9215));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 342L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9217));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 343L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9218));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 344L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9221));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 345L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9223));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 346L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9224));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 347L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9225));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 348L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9226));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 349L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9227));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 350L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9229));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 351L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9230));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 352L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9232));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 353L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9233));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 354L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9234));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 355L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9235));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 356L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9236));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 357L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9238));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 358L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9239));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 359L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9240));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 360L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9242));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 361L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9243));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 362L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9244));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 363L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9245));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 364L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9247));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 365L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9248));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 366L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9249));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 367L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9250));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 368L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9252));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 369L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9253));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 370L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9254));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 371L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9256));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 372L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9257));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 373L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9258));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 374L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9259));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 375L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9261));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 376L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9263));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 377L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9264));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 378L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9266));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 379L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9267));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 380L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9268));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 381L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9269));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 382L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9271));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 383L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9272));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 384L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9274));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 385L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9275));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 386L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9276));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 387L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9277));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 388L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9278));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 389L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9280));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 390L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9281));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 391L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9282));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 392L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9284));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 393L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9285));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 394L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9286));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 395L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9288));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 396L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9289));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 397L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9290));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 398L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9291));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 399L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9293));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 400L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9294));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 401L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9295));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 402L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9296));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 403L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9298));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 404L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9299));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 405L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9300));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 406L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9301));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 407L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9302));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 408L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9304));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 409L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9305));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 410L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9306));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 411L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9307));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 412L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9308));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 413L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9310));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 414L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9311));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 415L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9312));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 416L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9313));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 417L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9315));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 418L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9316));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 419L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9317));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 420L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9318));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 421L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9320));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 422L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9321));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 423L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9322));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 424L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9323));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 425L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9325));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 426L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9326));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 427L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9328));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 428L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9329));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 429L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9379));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 430L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9381));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 431L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9382));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 432L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9384));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 433L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9386));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 434L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9387));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 435L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9388));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 436L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9389));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 437L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9390));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 438L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9392));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 439L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9393));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 440L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9394));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 441L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9395));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 442L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9397));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 443L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9398));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 444L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9399));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 445L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9400));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 446L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9401));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 447L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9403));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 448L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9404));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 449L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9405));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 450L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9406));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 451L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9408));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 452L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9409));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 453L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9410));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 454L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9411));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 455L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9413));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 456L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9414));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 457L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9415));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 458L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9416));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 459L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9417));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 460L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9419));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 461L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9420));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 462L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9421));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 463L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9423));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 464L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9424));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 465L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9425));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 466L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9426));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 467L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9428));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 468L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9429));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 469L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9430));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 470L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9431));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 471L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9432));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 472L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9434));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 473L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9435));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 474L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9436));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 475L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9437));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 476L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9438));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 477L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9440));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 478L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9441));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 479L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9442));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 480L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9444));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 481L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9445));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 482L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9446));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 483L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9448));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 484L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9449));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 485L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9450));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 486L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9451));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 487L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9453));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 488L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9455));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 489L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9456));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 490L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9457));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 491L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9459));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 492L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9460));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 493L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9461));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 494L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9462));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 495L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9464));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 496L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9466));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 497L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9467));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 498L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9468));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 499L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9470));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 500L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9471));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 501L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9473));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 502L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9474));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 503L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9475));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 504L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9477));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 505L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9478));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 506L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9479));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 507L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9480));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 508L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9481));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 509L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9483));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 510L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9484));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 511L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9485));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 512L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9487));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 513L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9488));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 514L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9489));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 515L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9490));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 516L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9492));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 517L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9493));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 518L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9494));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 519L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9495));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 520L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9497));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 521L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9498));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 522L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9499));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 523L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9500));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 524L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9502));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 525L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9503));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 526L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9504));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 527L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9505));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 528L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9506));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 529L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9508));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 530L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9509));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 531L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9510));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 532L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9511));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 533L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9513));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 534L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9514));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 535L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9515));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 536L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9516));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 537L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9517));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 538L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9519));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 539L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9520));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 540L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9521));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 541L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9522));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 542L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9524));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 543L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9525));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 544L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9527));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 545L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9528));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 546L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9529));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 547L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9530));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 548L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9531));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 549L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9533));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 550L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9534));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 551L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9535));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 552L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9536));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 553L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9537));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 554L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9539));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 555L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9540));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 556L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9541));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 557L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9568));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 558L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9569));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 559L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9570));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 560L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9572));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 561L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9573));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 562L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9574));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 563L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9575));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 564L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9577));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 565L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9578));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 566L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9579));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 567L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9580));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 568L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9581));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 569L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9583));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 570L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9584));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 571L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9585));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 572L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9587));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 573L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9588));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 574L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9589));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 575L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9590));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 576L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9591));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 577L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9593));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 578L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9594));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 579L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9595));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 580L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9596));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 581L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9598));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 582L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9599));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 583L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9600));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 584L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9601));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 585L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9602));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 586L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9604));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 587L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9605));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 588L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9606));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 589L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9607));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 590L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9609));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 591L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9610));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 592L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9611));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 593L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9612));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 594L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9614));

            migrationBuilder.UpdateData(
                table: "Board",
                keyColumn: "Id",
                keyValue: 595L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 379, DateTimeKind.Local).AddTicks(9615));

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1L,
                column: "Create_At",
                value: new DateTime(2020, 8, 19, 11, 25, 56, 380, DateTimeKind.Local).AddTicks(3427));
        }
    }
}
