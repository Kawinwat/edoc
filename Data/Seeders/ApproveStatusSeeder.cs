﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Edocweb.Models;
namespace Edocweb.Data.Seeders
{
    public class ApproveStatusSeeder : IEntityTypeConfiguration<ApproveStatus>
    {
        public void Configure(EntityTypeBuilder<ApproveStatus> builder)
        {
            builder.HasData(
                        new ApproveStatus { Id = 1, Name = "อัพโหลดเอกสาร" },
                        new ApproveStatus { Id = 2, Name = "รอการอนุมัติ" },
                        new ApproveStatus { Id = 3, Name = "อนุมัติเรียบร้อย" }
            );
        }
    }
}

