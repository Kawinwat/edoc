using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Edocweb.ViewModels {
        public class DocumentFileViewModels {

                public long Id { get; set; }
                public string FileName { get; set; }
                public string PageNumber { get; set; }
                public long? DocumentFilePositionSignatureId { get; set; }
                public long DocumentFileStatusId { get; set; }
                public long DocumentId { get; set; }
        }
}