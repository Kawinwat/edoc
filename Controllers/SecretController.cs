﻿using Edocweb.Data;
using Edocweb.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EdocWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SecretController
    {
        private readonly ApplicationDbContext _context;
        public static IWebHostEnvironment _environment;
        public SecretController(ApplicationDbContext context, IWebHostEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }
        [HttpGet]
        public IEnumerable<Secret> Get()
        {
            var data = from P in _context.Secret
                       select P;
            return data;
        }
    }
}
