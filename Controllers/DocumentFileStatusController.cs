﻿
using Edocweb.Data;
using Edocweb.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EdocWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentFileStatusController
    {
        private readonly ApplicationDbContext _context;
        public static IWebHostEnvironment _environment;
        public DocumentFileStatusController(ApplicationDbContext context, IWebHostEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }
        [HttpGet]
        public IEnumerable<DocumentFileStatus> Get()
        {
            var data = from P in _context.DocumentFileStatus
                       select P;
            return data;
        }

        //[HttpGet]
        //public IActionResult GetAll()
        //{

        //    var data = _context.ApproveStatus;
        //    return Ok(data);
        //}
    }
}
