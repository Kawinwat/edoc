﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Edocweb.Data;
using Edocweb.Models;
using Edocweb.ViewModels;


namespace Edocweb.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class DocumentsFileController : ControllerBase {

        //เชื่อมต่อ database 
        private readonly ApplicationDbContext _context;
        public static IWebHostEnvironment _environment;
      
        public DocumentsFileController (ApplicationDbContext context, IWebHostEnvironment environment) {
            _context = context;
            _environment = environment;
        }

        [HttpGet]
         public IEnumerable<DocumentFile> Get() 
        {
            var data = from P in _context.DocumentFile
                               select P;
            return data;
        }

        [HttpGet("{id}")]
         public IActionResult GetItem (long id) 
        {
            var data = _context.DocumentFile
            .Find(id);
            return Ok (data);
        }

         [HttpGet ("[action]/{id}")]
         public IActionResult getstatus2 (long id) 
        {
            var data = _context.DocumentFile
            .Where(m => m.Id == id)
            .Where(m => m.DocumentFileStatusId == 2);
            return Ok (data);
        }

        [HttpPost]
        public void Post([FromForm] DocumentFileRequest request)
        {
        var data = new DocumentFilePositionSignature
            {
                 X1 = request.X1,
                 Y1 = request.Y1
              
            };

            _context.DocumentFilePositionSignature.Add(data);
            _context.SaveChanges();

             var data2 = new DocumentFile
            {
                DocumentFilePositionSignatureId = data.Id,
            };
            _context.DocumentFile.Add(data2);
            _context.SaveChanges();

            
        }

        [HttpPut("{id}")]
        public void Put([FromForm] DocumentFileRequest request,long id)
        {
            var data = _context.DocumentFile.Find(id);
            data.DocumentFileStatusId = 2; 
            _context.Entry(data).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();
        }
        


    }
}

public class DocumentFileRequest
{
    public long Id { get; set; }
    public string FileName { get; set; }
    public string PageNumber { get; set; }
    public long? DocumentFilePositionSignatureId { get; set; }            
    public long DocumentFileStatusId { get; set; }
    public long DocumentId { get; set; }

    public double X1 { get; set; }
    public double Y1 { get; set; }
               
}