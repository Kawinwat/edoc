﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Edocweb.Data;
using Edocweb.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace EdocWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentStatusController
    {
        private readonly ApplicationDbContext _context;
        public static IWebHostEnvironment _environment;
        public DocumentStatusController(ApplicationDbContext context, IWebHostEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }
        [HttpGet]
        public IEnumerable<DocumentStatus> Get()
        {
            var data = from P in _context.DocumentStatus
                       select P;
            return data;
        }
    }
}
